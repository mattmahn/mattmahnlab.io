const gulp = require('gulp');
const gulpRev = require('gulp-rev');
const revReplace = require('gulp-rev-replace');
const criticalStream = require('critical').stream;

const outDir = 'public';

/**
 * In-lines critical, above-the-fold CSS
 */
function critical() {
    return gulp.src(`${outDir}/**/*.html`)
        .pipe(criticalStream({
            base: outDir,
            ignore: [ '@font-face', /import url\(/ ],
            inline: true,
            minify: true,
        }))
        .pipe(gulp.dest(outDir));
}
const criticalTask = critical;
gulp.task('critical', criticalTask);

/**
 * Revisions all static assets
 */
function revStamp() {
    const assets = [
        `${outDir}/**/*.css`,
        `${outDir}/**/*.js`,

        `${outDir}/**/*.@(jpeg|jpg|png|svg)`
    ];
    return gulp.src(assets)
        .pipe(gulpRev())
        .pipe(gulp.dest(outDir))
        .pipe(gulpRev.manifest())
        .pipe(gulp.dest(outDir));
}
gulp.task('rev-stamp', revStamp);

/**
 * Map references to rev'd assets
 */
function rev() {
    const manifest = gulp.src(`${outDir}/rev-manifest.json`);
    return gulp.src(`${outDir}/**/*.html`)
        .pipe(revReplace({ manifest: manifest }))
        .pipe(gulp.dest(outDir));
}
const revTask = gulp.series(revStamp, rev);
gulp.task('rev', revTask);

gulp.task('default', gulp.series(revTask, critical));
