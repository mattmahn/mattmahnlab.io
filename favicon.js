let height = 32;
let width  = 32;
let center = width / 2,
    middle = height / 2;
let canvas = document.createElement('canvas');
canvas.height = height;
canvas.width = width;
let ctx = canvas.getContext('2d');
ctx.clearRect(0, 0, width, height);

// green NW circle-half
ctx.beginPath();
ctx.fillStyle = 'rgb(10, 207, 131)';
ctx.ellipse(center, middle, width/2, height/2, 3*Math.PI/4, 0, Math.PI);
ctx.fill();

// purple SE circle-half
ctx.beginPath();
ctx.fillStyle = 'rgb(162, 89, 255)';
ctx.ellipse(center, middle, width/2, height/2, 3*Math.PI/4, 0, Math.PI, true);
ctx.fill();

let b64Favicon = canvas.toDataURL('image/png');
let link = document.createElement('link');
link.rel = 'icon';
link.sizes = `${width}x${height}`;
link.href = b64Favicon;
document.head.appendChild(link);
