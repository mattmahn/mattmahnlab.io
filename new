#!/usr/bin/env bash
#
# Makes a new file for writing a blog post.

readonly base_dir=$(realpath -e "$(dirname "$0")")
readonly post_dir=$(realpath "${base_dir}/_posts")
readonly draft_dir=$(realpath "${base_dir}/_drafts")

print_help() {
  cat <<EOM
usage: new [options] <name of post>
options:
    -d             - create a new draft
    -e ext         - use the specified extension
    -h, --help     - print this message
EOM
}

TEMP=$(getopt -o 'de:hP:' -l 'help,publish' -n 'new' -- "$@")
if [[ $? -ne 0 ]]; then
  exit 1
fi
eval set -- "$TEMP"
unset TEMP

while true; do
  case "$1" in
    '-h'|'--help')
      print_help
      exit 0
      ;;
    '-d')
      dir="$draft_dir"
      shift
      ;;
    '-e')
      ext="$2"
      shift 2
      ;;
    '-P'|'--publish')
      draft="$2"
      sed -i "s/^:revdate: .*/:revdate: $(date --rfc-3339=seconds)/" "$draft"
      filename="$(date --iso-8601)-$(basename "$draft")"
      mv "$draft" "$post_dir/$filename"
      exit 0
      ;;
    '--')
      shift
      break
      ;;
  esac
done

# set defaults
dir="${dir:-$post_dir}"
ext="${ext:-adoc}"
filename="${*}.${ext}"

filename_slug="$(echo "$filename" | tr '[:upper:]' '[:lower:]' | tr ' ' '-')"

path=$(realpath -m "${dir}/${filename_slug}")
cat <<EOF > "$path"
$@
$(sed 's/./=/g' <<< "$@")
:revdate: $(date --rfc-3339=seconds)

EOF

if [[ "$EDITOR" ]]; then
  exec "$EDITOR" "$path"
else
  echo "set your \$EDITOR environment variable" >&2
  exit 3
fi
