---
layout: page
title: hello, internet
---

It seems that you've stumbled upon my little dust bunny in a corner of the vast internet.
Welcome!

You may notice that there isn't a whole lot here; that's because there isn't.
I'm not really sure what to put here, but I figured that any self-respecting software engineer ought to have their own domain.
So I bought one.
I've thought of putting a blog and/or some sort of portfolio, ~~but I'm not sure how I want to do that, so now you only get this lovely static HTML file.~~
I decided try out [Jekyll](http://jekyllrb.com/).

If you're into that sort of thing, you can check out my [GitHub](https://github.com/mattmahn), but you may <span title="Why?? Why did you notice?" style="cursor:help">notice</span> that this website is actually [hosted](https://gitlab.com/mattmahn/mattmahn.gitlab.io) with GitLab Pages.
This is because they allow me to use TLS with personal domains.


## bio?

I guess people usually put some sort of biography on their website.
Whelp, I received a B.S. in software engineering from the Milwaukee School of Engineering in 2017.
I have an interest in cybersecurity, sysadmin stuff, and casually moving my phalanges whilst staring into a screen (read: "gaming", but I guess writing software fits too).

P.S. I'm also one of those "nasty" Linux and open source aficionados.

P.P.S. By the way, my name's Matt Mahnke.


## internet stalking platforms*

- Bitbucket: [mattmahn](https://bitbucket.org/mattmahn){:rel='me'} (mostly just school projects)
- Email: [{{ site.email }}](mailto:{{ site.email }})
- GitHub: [mattmahn](https://github.com/mattmahn){:rel='me'}
- Keybase: [mattmahn](https://keybase.io/mattmahn){:rel='me'}
- LinkedIn: [matthewmahnke](https://www.linkedin.com/in/matthewmahnke){:rel='me'}
- PGP: <a class="tt" href="https://keybase.io/mattmahn/pgp_keys.asc?fingerprint=9ba080a794d2f46cac88ba6cbfab9c6e3ac28a56">BFAB 9C6E 3AC2 8A56</a>
- <span class="revoked">Old PGP: <span class="tt">157B 206B 0F06 3E1F</span></span>
- Twitter: [@matthewmahnke](https://twitter.com/matthewmahnke){:rel='me'}
