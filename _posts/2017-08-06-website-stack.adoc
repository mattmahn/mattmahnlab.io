Current stack
=============
:revdate: 2017-08-06 12:00:00-05:00

As you may know, Jekyll comes out of the box with Markdwon support through https://kramdown.gettalong.org/index.html[kramdown].
Now, I don't have anything against Markdown -- I think its probably the best lightweight markup language out there.
But I wanted something that had more fancy-pants features built-in, so I wouldn't have to remember the correct HTML to match my CSS rules for making the elements display correctly.
Some examples of built-in markups are: note admonition boxes, cited blockquotes, and http://plantuml.sourceforge.net/[PlantUML] diagrams.
(In setting up Jekyll to use Asciidoc, I also learned it supports mathematical formulas, Graphviz drawings, and other diagrams.)

Enabling Asciidoc support was fairly straightforward.
I only had to add the `jekyll-asciidoc` and `pygments.rb` gems.
However, since I also wanted to use the Graphviz, etc. diagrams, I also needed to add the `asciidoctor-diagram` gem as well as all of the diagramming programs' dependencies.
That's where the real trouble was.
Eventually I figured out I needed to install Java, Graphviz, Python, and the `{act,block,nw,seq}diag` pip packages, so that everything will build properly on GitLab Pages.

TIP: You can see the git diff for setting up Asciidoc https://gitlab.com/mattmahn/mattmahn.gitlab.io/compare/5af236f2\...ae20c4ee[here].
(I had poor commit hygiene so some extraneous changes are included too.)

Now that I have figured out the extra dependencies of using Asciidoc Diagram, I have another task on my todo list: creating a Docker image with all my dependencies already installed.
This way GitLab doesn't have to download and install all of them each time I push commits.

On the downside of getting all these features, I've noticed that generating the HTML files takes a bit longer.
Previously, when using kramdown, generating pages would take couple hundred milliseconds.
Now pages are taking close to one second to generate.
I suspect this may become inconvenient if this site ever grows to many pages, but it shouldn't matter much.
Since I configured GitLab pages to generate all the static HTML pages at deploy-time, there is only a delay in deploying new content, not serving it to visitors.
