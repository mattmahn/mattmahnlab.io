Test post [Asciidoc]
====================
:page-layout: post
:revdate: 2017-08-02 00:04:42 -05:00

I've decided to write posts/etc in Asciidoc henceforth.
Why?
Because Markdown is lacking some schanzy features, but its most redeeming quality is being able to embed arbitrary HTML.
However, Asciidoc has more built-in features, _and_ can embed arbitrary HTML!

I shall now show off some these feature-full markups. First up is the note, tip,
important, warning, and critical messages:

NOTE: This is a note message.

TIP: This is a tip.

IMPORTANT: This is important.

WARNING: This is warning you not to do a thing.

CAUTION: This is cautioning you a thing could happen.

Of course, there is also source code listing support using http://pygments.org/[Pygments]:
[source,python]
----
def fib(n):
    if n <= 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)

print(fib(2))  #=> 2
----

There's also math support via MathJax: latexmath:[\int \frac{1}{x} dx]
[latexmath]
++++
u(x) =
  \begin{cases}
    \exp{x} & x \geq 0 \\
    1       & x < 0
  \end{cases}
++++

There is also nifty built-in support for sidebar notes; it's around here
somewhere...

.Optional Sidebar Title
****
To the side! Over here more.

Pack my box with five dozen liquor jugs.
****

You can also put a ``literal'' block in:

....
*Literal* block

Use: workaround when literal paragraph (indented) like
  1. First
  2. Second
incorrectly processed as list.
....

Blockquotes are objectively better, since there is support for citing the
author and source:
[quote, The Meaning of "Hack", the Jargon File 4.4.7]
____
On November 20, 1982, MIT hacked the Harvard-Yale football game.
Just after Harvard's second touchdown against Yale, in the first quarter, a small black ball popped up out of the ground at the 40-yard line, and grew bigger, and bigger, and bigger.
The letters ``MIT'' appeared all over the ball.
As the players and officials stood around gawking, the ball grew to six feet in diameter and then burst with a bang and a cloud of white smoke.

The Boston Globe later reported: ``If you want to know the truth, MIT won The Game.''

The prank had taken weeks of careful planning by members of MIT's Delta Kappa Epsilon fraternity.
The device consisted of a weather balloon, a hydraulic ram powered by Freon gas to lift it out of the ground, and a vacuum-cleaner motor to inflate it.
They made eight separate expeditions to Harvard Stadium between 1 and 5 AM, locating an unused 110-volt circuit in the stadium and running buried wires from the stadium circuit to the 40-yard line, where they buried the balloon device.
When the time came to activate the device, two fraternity members had merely to flip a circuit breaker and push a plug into an outlet.

This stunt had all the earmarks of a perfect hack: surprise, publicity, the ingenious use of technology, safety, and harmlessness.
The use of manual control allowed the prank to be timed so as not to disrupt the game (it was set off between plays, so the outcome of the game would not be unduly affected).
The perpetrators had even thoughtfully attached a note to the balloon explaining that the device was not dangerous and contained no explosives.

Harvard president Derek Bok commented: ``They have an awful lot of clever people down there at MIT, and they did it again.''
President Paul E. Gray of MIT said: ``There is absolutely no truth to the rumor that I had anything to do with it, but I wish there were.''
____

There's even Q&As!
[qanda]
Will I use this Q&A feature?::
  Probably not.
Does it matter?::
  Nope.

Using http://asciidoctor.org/docs/asciidoctor-diagram[Asciidoctor Diagram] I
can even embed http://www.graphviz.org/[Graphviz] drawings...
[graphviz,dot-example,svg]
....
digraph g {
  a -> b
  b -> c
  c -> d
  d -> a
}
....

...http://ditaa.sourceforge.net/[Ditaa] drawings...
[ditaa,memory,png]
....
      +--------+
   0  |        |  <- start
      +--------+
   1  |        |  <- q  scans from start to end
      +--------+
      :  ..... |
      +--------+
      |        |  <- end
      +--------+  <-+
      |        |    |
      +--------+    | rest of the
      :  ..... |    | allocated memory
      +--------+    |
  n   |        |    |
      +--------+  <-+
....

...and http://plantuml.sourceforge.net/[PlantUML] diagrams...
[plantuml, diagram-classes, svg]
....
class BlockProcessor
class DiagramBlock
class DitaaBlock
class PlantUmlBlock

BlockProcessor <|-- DiagramBlock
DiagramBlock <|-- DitaaBlock
DiagramBlock <|-- PlantUmlBlock
....
[plantuml, use-case, svg]
....
User -> (Start)
User --> (Use the application) : A small label

:Main Admin: ---> (Use the application) : This is\nyet another\nlabel
....

...and finally http://blockdiag.com/en/nwdiag/index.html[nwdiag] pictograms:
[nwdiag,simple,svg]
....
nwdiag {
  network dmz {
      address = "210.x.x.x/24"

      web01 [address = "210.x.x.1"];
      web02 [address = "210.x.x.2"];
  }
  network internal {
      address = "172.x.x.x/24";

      web01 [address = "172.x.x.1"];
      web02 [address = "172.x.x.2"];
      db01;
      db02;
  }
}
....

There are even more supported by Asciidoctor diagrams!
